#!/usr/bin/perl

# Copyright (c) 2020-2023 Philip Hands <phil@hands.com>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use strict;
use warnings;
use Expect;
use Getopt::Std;
use Test2::V0;

my %opt=();
getopts("cfPh:i:p:st:u:x", \%opt) and defined($opt{h}) and defined($opt{u}) or
    die "Usage: [-f] [-s] [-x] [-c] [-P] [-u <user>] [-p <password>] [-i <id_file>] [-t target_path] -h <host>\n";

my $user_host = "$opt{u}\@$opt{h}";

my $timeout = 20 ;

sub test_ssh_copy_id {
  my $command;

  if (defined($opt{c})) {
    $command = 'KCOV_1TO8=1 /usr/bin/kcov --bash-dont-parse-binary-dir /tmp/kcov ' . $_[0] . ' 8>&1' ;
  } else {
    $command = $_[0];
  }

  # add environemnt settings, if specified as 2nd param
  if (defined $_[1]) {
    $command = $_[1] . " " . $command ;
  }

  my $exp = Expect->new;
  $exp->raw_pty(1);
  printf "about to run '%s'\n", $command ;
  $exp->spawn($command)
      or die "Cannot spawn ssh-copy-id: $!\n";

  my $spawn_ok = my $viable_found = 0;
  my ($matched_pattern_position, $error, $successfully_matching_string, $before_match,
      $after_match) = "";
  ($matched_pattern_position, $error, $successfully_matching_string,
   $before_match, $after_match) =
      $exp->expect($timeout,
                   ['INFO: ', sub {$spawn_ok = 1; exp_continue;}],
                   ['assword: ', sub {my $self = shift; $self->send("$opt{p}\n") ; exp_continue} ],
                   ['This service allows sftp connections only.', sub {note("    found: sftp only")}],
                   ['Would have added the following key\(s\):', sub {note("    found: would have added (-n - dry run)")}],
                   ['Number of key\(s\) added:', sub {note("    found: keys added")}],
                   ['WARNING: All keys were skipped', sub {note("    found: All skipped")}],
                   ['ERROR: Less dimwitted shell required.', sub {note("    found: dimwitted")}],
                   ['seems viable', sub { $viable_found = 1; note("    found: seems viable") ; exp_continue}],
                   ['ERROR: Too many arguments.', sub {note("    found: Too many arguments")}],
                   ['ERROR: -i option must not be ', sub {note("    found: Too many -i options")}],
                   ['ERROR: failed to open ID file', sub {note("    found: failed ... ID file")}],
                   ['Connection refused', sub {note("    found: Connection refused")}],
                   ['Usage: ', sub {note("    found: Usage")}],
                   [eof =>
                    sub {
                      if ($spawn_ok) {
                        diag("ERROR: premature EOF while running ssh-copy-id.");
                      } else {
                        diag("ERROR: could not spawn ssh-copy-id.");
                      }
                    }
                   ],
                   [
                    timeout =>
                    sub {
                      diag("timeout waiting for ssh-copy-id to produce output");
                      return -1;
                    }
                   ]
      );

  note ("result: ", $matched_pattern_position, " match [", $successfully_matching_string, "] after match [", $after_match, "]") ;
  $exp->soft_close() ;
  return $matched_pattern_position + 100 * $viable_found;
}

sub broken_opt_i($) {
  my $args = shift;
  if ($args =~ /^(.*-i\s*)(.*)$/) {
    return $1 . "this/should/break/things" . $2;
  } else {
    return $args . " -i this/should/break/things";
  }
}

sub broken_opt_p($) {
  my $args = shift;
  if ($args =~ /^(.*-p)[0-8](.*)$/) {
    return $1 . "9" . $2;
  } else {
    return $args . " -p9876";
  }
}

my $sftp_flag = my $sftp_comment = my $extra_args = '' ;

my $ssh_copy_id = './ssh-copy-id ';

if (defined($opt{s})) {
  $sftp_flag = '-s ' ;
  $sftp_comment = '(sftp)';
}

$extra_args .= "-x " if (defined($opt{x})) ;
$extra_args .= "-f " if (defined($opt{f})) ;
$extra_args .= "-i $opt{i} " if (defined($opt{i})) ;
# this allows one to pass extra options through to the ssh/sftp calls
$extra_args .= $ARGV[0] if defined($ARGV[0]) ;

SKIP: {
  skip "cannot provoke failure, as not an SFTP user", 1 unless defined($opt{s});

  is(test_ssh_copy_id($ssh_copy_id . $extra_args . ' ' . $user_host),    3, "get rejected ssh login when SFTP only");
}

is(test_ssh_copy_id($ssh_copy_id . $sftp_flag . $extra_args . ' -n ' . $user_host), 4, "keys not added [dry run] $sftp_comment");

if (defined($opt{t})) {
  # if we have -t, install key(s) to the wrong place first, since we won't thus stop the normal install
  is(test_ssh_copy_id($ssh_copy_id . $sftp_flag . $extra_args . " -t '$opt{t}'" . ' ' . $user_host), 5, "keys added $sftp_comment");
}
is(test_ssh_copy_id($ssh_copy_id . $sftp_flag . $extra_args . ' ' . $user_host), 5, "keys added $sftp_comment");

SKIP: {
  skip "-f specified, so duplicate key test is meaningless", 1 if defined($opt{f});

  is(test_ssh_copy_id($ssh_copy_id . $sftp_flag . $extra_args . ' ' . $user_host), 6, "All keys skipped $sftp_comment");
}


# 2 args, gives usage
is(test_ssh_copy_id($ssh_copy_id . $extra_args . " foo bar"), 9, "too many arguments");

# 2 -i's, gives usage
is(test_ssh_copy_id($ssh_copy_id . $extra_args . " -i ~testuser/.ssh/id_test1 -i ~testuser/.ssh/id_test1 " . $user_host), 10, "multiple -i options");

# -i to a non-existant path
is(test_ssh_copy_id($ssh_copy_id . broken_opt_i($extra_args) . $user_host), 11, "missing ID file");

is(test_ssh_copy_id($ssh_copy_id . '-x -h'), 13, "Usage");

# missing host, also gives usage
is(test_ssh_copy_id($ssh_copy_id . $extra_args), 13, "missing host -> Usage");

# specify the wrong port
is(test_ssh_copy_id($ssh_copy_id . $sftp_flag . broken_opt_p($extra_args) . ' ' . $user_host), 12, "wrong port - connection refused");

if (defined($opt{P})) {
  is(test_ssh_copy_id($ssh_copy_id . '-h', "FAKE_FAIL=1" ), 7, "dimwitted shell (when false=true)");
  is(test_ssh_copy_id($ssh_copy_id . '-h', "FAKE_FAIL=1 SANE_SH=/bin/dash"), 113, "seems viable (when false=true) & Usage");
}

done_testing;
